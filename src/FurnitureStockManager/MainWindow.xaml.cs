﻿using FurnitureStockManager.Classes;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace FurnitureStockManager
{
    public partial class MainWindow : Window
    {
        private ViewModel viewModel = new ViewModel();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void Window_Closing(object sender, CancelEventArgs e) => viewModel.SaveOnExit();

        private void Button_AddFurniture_Click(object sender, RoutedEventArgs e)
        {
            int firstFreeFurnitureId = viewModel.Furnitures.Max(f => f.Id) + 1;
            Func<string, bool> furnitureNameIsValid = name => !viewModel.Furnitures.Any(f => f.Name == name);
            var addWindow = new AddFurnitureWindow(firstFreeFurnitureId, furnitureNameIsValid);
            if (addWindow.ShowDialog() == true)
            {
                viewModel.Furnitures.Add(addWindow.Furniture);
            }
        }

        private void Button_DelFurniture_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.SelectedFurniture == null)
            {
                MessageBox.Show("Select a furniture first!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            viewModel.Furnitures.Remove(viewModel.SelectedFurniture);
        }

        private void Button_ExportFurnitures_Click(object sender, RoutedEventArgs e) => viewModel.ExportFurnitures();

        private void Button_AddAccessory_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.SelectedFurniture == null)
            {
                MessageBox.Show("Select a furniture first!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            var addWindow = new AddAccessoryWindow(viewModel.SelectedFurniture.Id);
            if (addWindow.ShowDialog() == true)
            {
                viewModel.SelectedFurniture.Accessories.Add(addWindow.Accessory);
            }
        }

        private void Button_DelAccessory_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.SelectedAccessory == null)
            {
                MessageBox.Show("Select an accessory first!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            viewModel.SelectedFurniture?.Accessories.Remove(viewModel.SelectedAccessory);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e) => viewModel.OrderByFurniturePrice();

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e) => viewModel.UndoOrderByFurniturePrice();
    }
}
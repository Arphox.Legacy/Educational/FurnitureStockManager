﻿using FurnitureStockManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace FurnitureStockManager
{
    public partial class AddFurnitureWindow : Window
    {
        private readonly Func<string, bool> furnitureNameIsValid;
        public Furniture Furniture { get; set; } = new Furniture();

        public AddFurnitureWindow(int firstFreeFurnitureId, Func<string, bool> furnitureNameIsValid)
        {
            InitializeComponent();
            DataContext = Furniture;
            Furniture.Id = firstFreeFurnitureId;
            Furniture.Size = new FurnitureSize();
            this.furnitureNameIsValid = furnitureNameIsValid;

            comboBox_Material.ItemsSource = Enum.GetValues(typeof(Material)).Cast<Material>();
            comboBox_Placement.ItemsSource = Enum.GetValues(typeof(Placement)).Cast<Placement>();
            Furniture.Size.X = Furniture.Size.Y = Furniture.Size.Z = 10;
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            List<string> validationMessages = Validate();
            if (validationMessages.Count > 0)
            {
                MessageBox.Show(string.Join(Environment.NewLine, validationMessages), "Validation error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DialogResult = true;
            Close();
        }

        private List<string> Validate()
        {
            List<string> messages = new List<string>();

            if (string.IsNullOrWhiteSpace(Furniture.Name))
                messages.Add("The name cannot be null, empty or contain only whitespace characters.");
            if (!furnitureNameIsValid(Furniture.Name))
                messages.Add("This furniture name is already taken!");
            if (Furniture.Size.X < 10)
                messages.Add("Size.X is expected to be at least 10.");
            if (Furniture.Size.Y < 10)
                messages.Add("Size.Y is expected to be at least 10.");
            if (Furniture.Size.Z < 10)
                messages.Add("Size.Z is expected to be at least 10.");
            if (Furniture.Amount < 0)
                messages.Add("Invalid amount! (Has to be 0 or greater)");

            return messages;
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
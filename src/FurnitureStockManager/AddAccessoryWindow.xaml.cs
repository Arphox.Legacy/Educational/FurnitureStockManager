﻿using FurnitureStockManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace FurnitureStockManager
{
    public partial class AddAccessoryWindow : Window
    {
        public Accessory Accessory { get; set; } = new Accessory();

        public AddAccessoryWindow(int furnitureId)
        {
            InitializeComponent();
            Accessory.FurnitureId = furnitureId;
            DataContext = Accessory;

            comboBox_Material.ItemsSource = Enum.GetValues(typeof(Material)).Cast<Material>();
            comboBox_AccessoryType.ItemsSource = Enum.GetValues(typeof(AccessoryType)).Cast<AccessoryType>();;
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            List<string> validationMessages = Validate();
            if (validationMessages.Count > 0)
            {
                MessageBox.Show(string.Join(Environment.NewLine, validationMessages), "Validation error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DialogResult = true;
            Close();
        }

        private List<string> Validate()
        {
            List<string> messages = new List<string>();

            if (Accessory.Price < 10)
                messages.Add("Price is expected to be at least 10.");
            if (Accessory.Price < 0)
                messages.Add("Invalid Price! (Has to be 10 or greater)");

            return messages;
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
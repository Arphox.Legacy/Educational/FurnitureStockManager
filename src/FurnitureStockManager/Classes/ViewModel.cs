﻿using FurnitureStockManager.Model;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Forms;

namespace FurnitureStockManager.Classes
{
    public sealed class ViewModel
    {
        public ObservableCollection<Furniture> Furnitures { get; } = new ObservableCollection<Furniture>();
        public ICollectionView FurnituresView { get; }

        public Furniture SelectedFurniture { get; set; }
        public Accessory SelectedAccessory { get; set; }

        public ViewModel()
        {
            // EXAMPLE FURNITURES
            var fur1 = new Furniture(0, "Bed", new FurnitureSize(90, 50, 185), Material.Wooden, Placement.LivingRoom, 5);
            fur1.AddAccessory(new Accessory(AccessoryType.Extra, 1000, Material.Wooden));
            fur1.AddAccessory(new Accessory(AccessoryType.Holder, 500, Material.Metal));
            Furnitures.Add(fur1);

            var fur2 = new Furniture(1, "Chair", new FurnitureSize(50, 50, 120), Material.Metal, Placement.Kitchen, 20);
            fur2.AddAccessory(new Accessory(AccessoryType.Decoration, 2000, Material.Other));
            Furnitures.Add(fur2);

            var fur3 = new Furniture(2, "Table", new FurnitureSize(120, 140, 90), Material.Other, Placement.Kitchen, 12);
            fur3.AddAccessory(new Accessory(AccessoryType.Extra, 500, Material.Other));
            fur3.AddAccessory(new Accessory(AccessoryType.Extra, 500, Material.Other));
            fur3.AddAccessory(new Accessory(AccessoryType.Extra, 500, Material.Other));
            fur3.AddAccessory(new Accessory(AccessoryType.Extra, 500, Material.Other));
            Furnitures.Add(fur3);

            FurnituresView = CollectionViewSource.GetDefaultView(Furnitures);
        }

        public void SaveOnExit() => FileManager.SaveAllToCsv(Furnitures);

        public void ExportFurnitures()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file (*.txt)|*.txt";
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileManager.SaveReport(Furnitures, saveFileDialog.FileName);
            }
        }

        internal void OrderByFurniturePrice() => FurnituresView.SortDescriptions.Add(new SortDescription(nameof(Furniture.Price), ListSortDirection.Ascending));
        internal void UndoOrderByFurniturePrice() => FurnituresView.SortDescriptions.Clear();
    }
}
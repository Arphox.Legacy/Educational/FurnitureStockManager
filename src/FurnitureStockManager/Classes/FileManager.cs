﻿using FurnitureStockManager.Model;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FurnitureStockManager.Classes
{
    internal static class FileManager
    {
        internal static void SaveAllToCsv(IEnumerable<Furniture> furnitures)
        {
            File.WriteAllLines("furnitures.csv", furnitures.Select(f => f.SerializeToCsv()));
            File.WriteAllLines("accessories.csv", furnitures.SelectMany(f => f.Accessories).Select(a => a.SerializeToCsv()));
        }

        internal static void SaveReport(IEnumerable<Furniture> furnitures, string filePath)
        {
            int averageSize = (int)furnitures.Average(f => f.Size.X * f.Size.Y * f.Size.Z);
            int averagePrice = (int)furnitures.Average(f => f.CalculatePrice());
            File.WriteAllText(filePath, 
                $"Average size of furnitures: {averageSize} cm^2\r\n" +
                $"Average price of furnitures: {averagePrice} Ft.");
        }
    }
}
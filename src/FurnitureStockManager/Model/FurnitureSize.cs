﻿using System;

namespace FurnitureStockManager.Model
{
    public sealed class FurnitureSize
    {
        private const int MinSize = 10;

        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public FurnitureSize()
        {
        }

        public FurnitureSize(int x, int y, int z)
        {
            ThrowIfLessThanMinimumSize(nameof(x), x);
            ThrowIfLessThanMinimumSize(nameof(y), y);
            ThrowIfLessThanMinimumSize(nameof(z), z);

            X = x;
            Y = y;
            Z = z;
        }

        private void ThrowIfLessThanMinimumSize(string paramName, int value)
        {
            if (value < MinSize)
                throw new ArgumentOutOfRangeException(paramName, $"Expecting {paramName} to be at lest {MinSize}.");
        }

        internal string SerializeToCsv() => $"{X},{Y},{Z}";
    }
}
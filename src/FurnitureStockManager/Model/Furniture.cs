﻿using System.Collections.ObjectModel;
using System.Linq;

namespace FurnitureStockManager.Model
{
    public sealed class Furniture
    {
        public int Id { get; set; }
        public string Name { get; set; } = "MyFurniture";
        public FurnitureSize Size { get; set; } = new FurnitureSize(10, 20, 30);
        public Material Material { get; set; } = Material.Wooden;
        public Placement Placement { get; set; } = Placement.Bathroom;
        public int Amount { get; set; } = 1;
        public ObservableCollection<Accessory> Accessories { get; set; } = new ObservableCollection<Accessory>();
        public int Price => CalculatePrice();

        public Furniture()
        {
        }

        public Furniture(int id, string name, FurnitureSize size, Material material, Placement placement, int amount)
        {
            Id = id;
            Name = name;
            Size = size;
            Material = material;
            Placement = placement;
            Amount = amount;
        }

        public void AddAccessory(Accessory accessory)
        {
            accessory.FurnitureId = Id;
            Accessories.Add(accessory);
        }

        public int CalculatePrice()
        {
            int basePrice = Size.X * Size.Y * Size.Z * 100;
            int accessoryTotalPrice = Accessories.Sum(a => a.Price);
            return basePrice + accessoryTotalPrice;
        }

        internal string SerializeToCsv() => string.Join(",", Id, Name, Size.SerializeToCsv(), Material.ToString(), Placement.ToString(), Amount);
        public override string ToString() => SerializeToCsv();
    }
}
﻿namespace FurnitureStockManager.Model
{
    public enum Material
    {
        Wooden = 1,
        Metal = 2,
        Other = 3
    }
}
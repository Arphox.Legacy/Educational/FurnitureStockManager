﻿namespace FurnitureStockManager.Model
{
    public enum Placement
    {
        Bathroom = 1,
        LivingRoom = 2,
        Kitchen = 3
    }
}
﻿namespace FurnitureStockManager.Model
{
    public enum AccessoryType
    {
        Holder = 1,
        Extra = 2,
        Decoration = 3
    }
}
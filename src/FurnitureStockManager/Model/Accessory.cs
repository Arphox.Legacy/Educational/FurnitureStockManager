﻿namespace FurnitureStockManager.Model
{
    public sealed class Accessory
    {
        public int FurnitureId { get; set; }
        public AccessoryType AccessoryType { get; set; } = AccessoryType.Extra;
        public int Price { get; set; } = 10;
        public Material Material { get; set; } = Material.Other;

        public Accessory()
        {
        }

        public Accessory(AccessoryType accessoryType, int price, Material material)
        {
            AccessoryType = accessoryType;
            Price = price;
            Material = material;
        }

        internal string SerializeToCsv() => string.Join(",", FurnitureId, AccessoryType.ToString(), Price, Material.ToString());
        public override string ToString() => SerializeToCsv();
    }
}